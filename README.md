## Fonctionnalités

* Jupyter : Application Web qui permet de créer des notebooks, c'est-à-dire
des documents multimédias intégrant du code exécutable.

* Jupyterhub : Fournit un environnement multi-utilisateurs pour Jupyter

* Kernel Ipython : Python3 + SQL via ipython-sql + Processing Py

* Kernel IOcaml

* Serveur Web multi-utilisateurs.


## Installation en local sur une virtualbox Ubuntu 16.04

1. Installer Vagrant et Ansible sur votre machine.

2. Créer la virtualbox en exécutant `vagrant up`.

3. Exécuter l'instruction `vagrant ssh` pour vous loguer en ssh sur la virtualbox.

4. Accèder au serveur à partir de `https://localhost:4433`.

Le mot de passe de l'utilisateur `vagrant` est `vagrant`.


## Installation sur une machine distante Ubuntu 16.04

1. Préparer une machine hôte Ubuntu 16.04 pour le serveur jhub.

2. Créer, si vous n'en disposez pas, d'une paire de clés pour la connexion ssh.

3. Installer Ansible sur votre ordinateur.

4. Ajouter l'adresse IP ou le nom de domaine de votre serveur dans le fichier `/etc/ansible/hosts` :
    ```
    [jhubserver]
    exemple.serveur.net
    ```

5. Copier votre clé publique dans le dossier personnel de `root`
    Par exemple : `ssh-copy-id -i ~/.ssh/id_rsa.pub root@exemple.serveur.net`

6. Créer le compte administrateur et y-déposer votre clé publique
  * Éditer le fichier `create-admin-user.yml` et modifier la section `vars` pour y renseigner le nom du compte administrateur ainsi que le nom du fichier contenant votre clé publique pour la connection ssl.
  * Le mot de passe provisoire est `turing` (à changer !). Ce mot de passe est utile pour se connecter à Jupyter sous le compte administrateur.
  * Exécuter `ansible-playbook create-admin-user.yml`.

7. Éditer le playbook `deploy.yml` :
  * Remplacer le nom `vagrant` par le nom du compte administrateur (deux endroits)
  * Si vous disposez d'un certificat ssl, remplacer les noms des fichiers `selfcert.pem` et `selfprivkey.pem` par vos propres fichiers. Placer ces fichiers dans le dossier `files`.
  * Indiquer éventuellement le nom d'un fichier image pour le logo du site. Le fichier doit être placé dans le sous-dossier `files`.

8. Installer jhub sur le serveur :
    ```
    ansible-playbook -l jhubserver deploy.yml
    ```

9. Accèder au serveur jhub : `https://exemple.serveur.net`.


## Utilisation du serveur

1. Créer et supprimer des utilisateurs.
  * La commande `juseradd alan` crée et configure un compte pour l'utilisateur `alan`. Le mot de passe provisoire est `turing` (à changer !). 
  * La commande `juserdel alan` supprime le compte utilisateur `alan` ainsi que son dossier personnel.

2. Supervision du serveur
  * `sudo service supervisor start` démarre `supervisor` qui démarre à son tour automatiquement `jupyterhub`. Ceci se fait automatiquement au boot du serveur.
  * La commande `sudo supervisorctl stop jupyterhub` permet d'arrêter `jupyterhub`.
  * La commande `sudo supervisorctl start jupyterhub` permet de le redémarrer.

3. En cas de problème, consulter les fichiers logs de `nginx` et `jupyterhub` dans le sous-dossier `jhub` du compte administrateur.

4. Si vous ne l'avez pas encore fait, vous pouvez générer un certificat ssl, en utilisant par exemple `letsencrypt`. Les fichiers doivent être placés dans le
sous-dossier `jhub` du dossier personnel du compte administrateur sous les noms `cert.pem` et `privkey.pem`.

5. Serveur Web

    Seul le sous-dossier `pub` du dossier personnel d'un utilisateur est visible par les autres utilisateurs.
    Ce sous-dossier `pub` est d'ailleurs totalement public :
    la requête `https://exemple.serveur.net/~alan/page.html` sert le fichier 
    `/home/alan/pub/page.html`.
